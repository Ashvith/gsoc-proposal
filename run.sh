#/bash/sh

mkdir -p generated

pandoc ${1}.md \
    metadata.yaml \
    -o generated/${1}.pdf \
    --pdf-engine=xelatex