# Project

[`PerfChecker.jl`](https://github.com/JuliaConstraints/PerfChecker.jl) is a set of performance-checking tools for Julia packages to create a performance environment, similar to a test environment.
The basic feature of the package is:

- Version independence during the evaluation of performance
- Allocation and benchmark checks
- Visualisation for multiple targeted versions

Some of the potential hurdles in the project may include getting familiarised with performance metrics and getting used to programming paradigms like metaprogramming, constraint programming, and functional programming.

There are several project ideas that have to be worked on to move this project to a stable API, which have been mentioned in the [Deliverables](#deliverables) section. [`PerfChecker.jl`](https://github.com/JuliaConstraints/PerfChecker.jl) is maintained by [`JuliaConstraints`](https://juliaconstraints.github.io/), an organization supporting packages for constraint programming in Julia.

# About me

My name is Ashvith Shetty. I am from Mangaluru, a port city in the state of Karnataka, India.

I am interested in pursuing higher studies in the domains of deep learning, robotics, and computer vision. At the moment, I am taking a sabbatical to prepare for postgraduate studies.

Most of my contributions to open source were related to Ruby projects on GitHub, but I've recently started contributing to GitLab. Some of the sample PRs are:

- [Record timestamp of status updates for container repositories](https://gitlab.com/gitlab-org/gitlab/-/issues/378384)
- [Rubocop: Extend Graphql/Descriptions cop to ban descriptions with "this" (lowercase) in them](https://gitlab.com/gitlab-org/gitlab/-/issues/383721)
- [Indexer FROM_SHA and TO_SHA should be sent as command line flags](https://gitlab.com/gitlab-org/gitlab/-/issues/372873)
- [RuboCop: Fix infinite loop in autocorrection in Graphql/Descriptions](https://gitlab.com/gitlab-org/gitlab/-/issues/383721)

I am interested in Julia because it seems to resolve some of the issues I have experienced with Python. It is faster and has a better package management solution, which is a convincing reason to embrace the language. Apart from that, there is also this excitement of exploring and learning new languages. I don't have a lot of experience with Julia, but I've submitted a few PRs for Julia packages [#1](https://github.com/JuliaStats/GLM.jl/pull/521) and [#2](https://github.com/JuliaConstraints/PerfChecker.jl/pull/20), and I believe that I can translate my previous open source experiences before the beginning of this programme.

I am a 2022 graduate of NMAM Institute of Technology from Udupi, Karnataka. My degree is a Bachelor's in Engineering (specialisation in Computer Science), so I believe that I might have the appropriate background for taking part in this programme.

# Code portfolio

Some of my personal repositories that I am interested in sharing are:

### [Phonebook](https://gitlab.com/Ashvith/phonebook)

This projects is a simple phonebook web app using Ruby, Rails, Propshaft, Devise, Sass, esbuild, Bulma, and PostgreSQL. This webapp supports user management and data isolation for every user.

### [ASCII Renderer](https://gitlab.com/Ashvith/ascii-renderer)

Inspired by the CLI-based [Donut math](https://www.a1k0n.net/2011/07/20/donut-math.html) and [Spinning Cube](https://youtu.be/p09i_hoFdd0). This project adds improvements to the CLI app by making it more responsive and removing jagged frames.

# Deliverables

Currently, the package still does not have a stable API, and the project maintainers are looking to integrate a whole bunch of features, of which I've chosen a few that I believe are suitable for me:

|Target|Ideas|Time taken (&#x2248; in hours)|
|:-|:------|---:|
|&#x2713;|Support for *PerfCheck* environment, similar to [`Test.jl`](https://github.com/JuliaLang/julia/tree/master/stdlib/Test) and [`Pkg.jl`](https://github.com/JuliaLang/Pkg.jl)|40|
|&#x2713;|Syntactical sugar support (`@bench`, `@alloc` and `@profile`), similar to [`Test.jl`](https://github.com/JuliaLang/julia/tree/master/stdlib/Test) and [`Pkg.jl`](https://github.com/JuliaLang/Pkg.jl)|40|
|&#x2713;|Interactive GUI using [`Makie.jl`](https://github.com/MakieOrg/Makie.jl)|60|
|&#x2713;|Support for handling different Julia and package versions, integrating with [`juliaup`](https://github.com/JuliaLang/juliaup)|40|
||Automatically generate version parametric space for both packages and Julia|30|
|&#x2713;|Descriptive plot captions|30|
||Improve automatic plotting through custom plots and visualisations|30|
||Enhance allocation vs space trade-off analysis, providing insight for developers to optimise their code|40|
|||**210 (for target ideas); 310 (for all ideas)**|

In the worst-case scenario, it will probably take more than the time allotted for large projects, and we might have to focus on the target ideas. Each of these ideas can be allotted time, as shown below in the [Timeline](#timeline) section.

# Timeline

The total length of the fellowship is about 22 weeks, and this timeline should provide an insight on how the project ideas will be broken down. The general idea is to work first on basic features to get used to the project structure, and later, start with advanced features:

## Before April 4 (2 weeks):

- Start adding [`PerfChecker.jl`](https://github.com/JuliaConstraints/PerfChecker.jl) to Julia projects and evaluating their performance.
- Attempt to submit at least one merge request to integrate [`PerfChecker.jl`](https://github.com/JuliaConstraints/PerfChecker.jl) with a project.

## April 5 - May 29 (3½ weeks):

- Take a deep dive into intermediate Julia and familiarise yourself with the language.
- Bond with the mentor(s) and the community by understanding the project structure, and reading the documentation.
- Understanding the development pipeline and contribution convention.
- Contribute to a few more projects in Julia.
- Learn about metaprogramming, GUI, and REPL, as well as coverage, benchmarks, and profiling.

## May 29 - June 18 (3 weeks):

- Refer to [`Test.jl`](https://github.com/JuliaLang/julia/tree/master/stdlib/Test) or [`Pkg.jl`](https://github.com/JuliaLang/Pkg.jl) to understand how syntatic sugars were used.
- Make use of the information above to add syntactical sugar to the package, namely `@bench`, `@alloc` and `@profile`.

## June 19 - July 9 (3 weeks):

- Learn about environments in Julia and refer to example packages like [`Test.jl`](https://github.com/JuliaLang/julia/tree/master/stdlib/Test) or [`Pkg.jl`](https://github.com/JuliaLang/Pkg.jl).
- Start working on the implementation of environment for *PerfCheck*.

## July 10 - August 6 (4 weeks):

- Play around with [`Makie.jl`](https://github.com/MakieOrg/Makie.jl), understand the documentation, and work on a few demo projects.
- Implement an interactive GUI using [`Makie.jl`](https://github.com/MakieOrg/Makie.jl).

## August 7 - August 27 (3 weeks):

- Understand what [`juliaup`](https://github.com/JuliaLang/juliaup) is, and how it works.
- Make sure to understand the problems in dealing with Windows and UNIX-like systems, and how to handle such cases.
- Support handling different Julia and package versions and integrate using [`juliaup`](https://github.com/JuliaLang/juliaup).

## August 28 - September 10  (2 weeks):

- Watch for possible implementations in other respective libraries/languages and work on improving the same.
- Automatically generate version parametric space for both packages and Julia.

## September 11 - September 24 (2 weeks):

- Discuss with the mentor where to focus while working on descriptive plot captions.
- Work on the implementation of descriptive plot captions.

## September 25 - October 8 (2 weeks):

- Learn about the default plot library used in [`PerfChecker.jl`](https://github.com/JuliaConstraints/PerfChecker.jl) ([`PGFPlotsX.jl`](https://github.com/KristofferC/PGFPlotsX.jl)).
- Request a discussion session with the mentor.
- Attempt to improve automatic plotting through custom plots and visualisations.

## October 9 - October 29 (3 weeks):

- Understand the problem and discuss it with the mentor.
- Enhance allocation vs. space trade-off analysis, providing insight for developers to optimise their code.

## October 30 - November 5 (1 week):

- Work on incomplete ideas that can be wound up in a week.
- Finish up the final remaining task for the GSoC submission.
- Reach out to the mentor to inform them of the same.